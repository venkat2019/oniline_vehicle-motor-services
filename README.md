Online vehcile Motor Service : 
...................................
        
About the Vehicle Service Management System :
.................................................

This project has a Public Module and Admin Module. The Admin module is the side of the project where can the management manage all the data recorded/will be recorded on the system. The admin can manage the list of vehicle categories that does the shop handles and other important and relative data for this project such as the mechanic list and service requests list. On the admin side. On the public side, the clients can explore the list of services does the company/shops provide. The client can submit their service request at this side and the submitted request will be marked as pending. This simple project also generates a date-wise printable service request report


Project Features :
...................
Admin Panel :
Secure Login
Manage Service Requests List (CRUD)
Generate a Date-wise and Printable Report
Manage User List 
Manage All branch details & Materails 

Public Side :
User Login & Registration 
Display the list of vehicle types/categories that does the shop's accommodates
Display the services that do the shops provide.
Submit Service Requests (CRUD)
Service status chek & Billing details 
Chek branch address details 


Technologies used for this project : 
........................................
JAVA  
SPRING FRAMEWORK  
SPRING BOOT 
SPRING DATA JPA 
HIBERNATE 
MYSQL ( DATABASE) 
Maveen  
SPRING TOOL SUITE 
POST MAN ( TESTING API’S) 






