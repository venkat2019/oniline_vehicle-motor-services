package com.vehicle.service.controller;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThrows;
import static org.mockito.Mockito.when;

import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;

import com.vehicle.services.controller.Branch;
import com.vehicle.services.exceptions.DataNotFoundException;
import com.vehicle.services.model.Branches;
import com.vehicle.services.repository.BranchDao;
@SpringBootTest(classes = {BranchTest.class})
public class BranchTest {

	@Mock
	BranchDao branchdao;
	
	@InjectMocks
	Branch branchcontroller;

	@Test
	@Order(2)
	public void testGetBranchDetails() {
		Branches getbranch = new Branches(101, "kothapet",
				"nagol", 9181798);
		String branch = "kothapet";
		when(branchdao.findBybranch(branch)).thenReturn(getbranch);
		assertEquals(getbranch, branchcontroller.getbranchaddress(branch));
		
    when(branchdao.findBybranch(branch)).thenReturn(null);
		
		assertThrows(DataNotFoundException.class,()->{branchcontroller.getbranchaddress(branch);});
	
		
		

	}

	

}
