package com.vehicle.service.controller;

import static org.junit.Assert.assertEquals;

import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.springframework.boot.test.context.SpringBootTest;

import com.vehicle.services.controller.ServicesProviding;

@SpringBootTest(classes = {ServiceProvidingTest.class})
public class ServiceProvidingTest {

	@InjectMocks
	ServicesProviding serviceproviding;
	
	
	@Test
	public void testServiceProviding() {
		
		  String services =  new ServicesProviding().GetServicesproviding();
		 assertEquals(services, "Engine Oil change , filter cleaing , chekking ligts , tyres , ALL THINGS do it here ....");
	}
}
