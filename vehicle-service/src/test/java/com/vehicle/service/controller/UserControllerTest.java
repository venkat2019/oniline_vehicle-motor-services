package com.vehicle.service.controller;
import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;

import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import com.vehicle.services.controller.UserController;
import com.vehicle.services.header.HeaderGenerator;
import com.vehicle.services.model.User;
import com.vehicle.services.service.UserService;

@SpringBootTest(classes = {UserControllerTest.class})
public class UserControllerTest {

	@Mock
	private UserService userService;

	@Mock
	private HeaderGenerator headerGenerator;

	@InjectMocks
	private UserController userController;

	User user = new User(101, "venkat", "reddy", "venkat reddy", "@gmail.com", "password",
			 "user","8186851416");

	@Test
	public void testResgister() {
		userController.userRegistration(user);
		verify(userService , times(1)).saveUser(user);;
		assertEquals("hi registered", userController.userRegistration(user));

	}

	@Test
	public void testLogin() {

		when(userService.signin(user.getUserName(), user.getPassword())).thenReturn("Login");
		ResponseEntity<String> res = userController.loginByUserName(user.getUserName(), user.getPassword());
		assertEquals(HttpStatus.OK, res.getStatusCode());
		assertEquals("Login", res.getBody());
		when(userService.signin(user.getUserName(), user.getPassword())).thenReturn("");
		ResponseEntity<String> res1 = userController.loginByUserName(user.getUserName(), user.getPassword());
		assertEquals(HttpStatus.NOT_FOUND, res1.getStatusCode());
		assertEquals("token not generated", res1.getBody());
	}

}