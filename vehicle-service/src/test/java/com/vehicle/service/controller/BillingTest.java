package com.vehicle.service.controller;

import static org.hamcrest.CoreMatchers.any;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThrows;
import static org.mockito.Mockito.when;

import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;

import com.vehicle.services.controller.BillingController;
import com.vehicle.services.exceptions.DataNotFoundException;
import com.vehicle.services.model.Billing;
import com.vehicle.services.repository.BillingDao;

@SpringBootTest(classes = {BillingTest.class})
public class BillingTest {

	@Mock
	BillingDao billingdao ;
	
	@InjectMocks 
	BillingController billingcontroller;
	
	@Test
	@Order(2)
	public void testGetBillingDetails() {

		Billing userbill = new Billing(101, "ts78tv1234", "fz", "no", 320, "uppal", 30, 0, 650, null);
		String vehicleregistrationnumber = "ts78tv1234";
		when(billingdao.findById(vehicleregistrationnumber)).thenReturn(userbill);// Mocking
		assertEquals(userbill, billingcontroller.fetching(vehicleregistrationnumber));
		when(billingdao.findById(vehicleregistrationnumber)).thenReturn(null);
		assertEquals(any(DataNotFoundException.class), billingcontroller.fetching(vehicleregistrationnumber));
		when(billingdao.findById(vehicleregistrationnumber)).thenReturn(null);

		assertThrows(DataNotFoundException.class, () -> {
			billingcontroller.fetching(vehicleregistrationnumber);
		});
	}
}
