package com.vehicle.service.controller;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThrows;
import static org.mockito.Mockito.when;

import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;
import com.vehicle.services.controller.Service;
import com.vehicle.services.exceptions.DataNotFoundException;
import com.vehicle.services.model.ServiceBook;
import com.vehicle.services.service.ServiceBookingService;
@SpringBootTest(classes = {ServiceTest.class})
public class ServiceTest {

	@Mock
	ServiceBookingService servicebooking;

	@InjectMocks
	Service servicecontroller;

	@Test
	@Order(1)
	public void testServiceBook() {
		ServiceBook vehicleservice = new ServiceBook(101, 1, "tso5tv1212", "r15", "Yamaha", "nagol", "2 wheeler",
				"engine chek up", null, null, null, "nagol", "please chek engine cleary");
		when(servicebooking.bookvehiclesservice(vehicleservice)).thenReturn(vehicleservice);// Mocking
		assertEquals("your vehicle service booking confim", servicecontroller.servicebooking(vehicleservice));

	}

	@Test
	@Order(2)
	public void testVehicleServiceStatus() {
		ServiceBook servicestatus = new ServiceBook(101, 1, "tso5tv1212", "r15", "Yamaha", "nagol", "2 wheeler",
				"engine chek up", null, null, null, "nagol", "please chek engine cleary");
		String vehiclenumber = "ts15ab9090";
		when(servicebooking.getdetails(vehiclenumber)).thenReturn(servicestatus);// Mocking
		assertEquals(servicestatus, servicecontroller.getdetails(vehiclenumber));
		when(servicebooking.getdetails(vehiclenumber)).thenReturn(null);
		assertThrows(DataNotFoundException.class, () -> {
			servicecontroller.getdetails(vehiclenumber);
		});
	}
	
	@Test
	@Order(3)
	public void testUpdateBilling() {
		ServiceBook servicestatus = new ServiceBook(101, 1, "tso5tv1212", "r15", "Yamaha", "nagol", "2 wheeler",
				"engine chek up", null, null, null, "nagol", "please chek engine cleary");
		String vehilcenumber = "ts78tv1234";
		when(servicebooking.update(servicestatus , vehilcenumber)).thenReturn(servicestatus);// Mocking
		assertEquals("updated successfully", servicecontroller.updatedeatilsById(vehilcenumber, servicestatus));
		when(servicebooking.update(servicestatus, vehilcenumber)).thenReturn(null);
		assertThrows(DataNotFoundException.class, () -> {
			servicecontroller.updatedeatilsById(vehilcenumber, servicestatus);
		});
	}
	
	@Test
	 @Order(4)
	public void testCancelService() {
		ServiceBook servicecancel = new ServiceBook(101, 1, "tso5tv1212", "r15", "Yamaha", "nagol", "2 wheeler",
				"engine chek up", null, null, null, "nagol", "please chek engine cleary");
		String vehilcenumber = "ts78tv1234";
		when(servicebooking.cancelServiceRequest(vehilcenumber)).thenReturn(servicecancel);
		assertEquals("you have canceled your service request", servicecontroller.cancelService(vehilcenumber));
		when(servicebooking.cancelServiceRequest(vehilcenumber)).thenReturn(null);
		assertThrows(DataNotFoundException.class, () -> {
			servicecontroller.cancelService(vehilcenumber);
		});
	}
	
	
}
