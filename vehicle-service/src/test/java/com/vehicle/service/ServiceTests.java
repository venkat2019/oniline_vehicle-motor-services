package com.vehicle.service;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThrows;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.sql.Date;

import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;

import com.vehicle.services.exceptions.DataNotFoundException;
import com.vehicle.services.model.ServiceBook;
import com.vehicle.services.repository.ServiceDao;
import com.vehicle.services.service.ServiceBookingService;


@SpringBootTest(classes = { ServiceTests.class })
public class ServiceTests {

	@InjectMocks
	ServiceBookingService servicebookingservice;

	@Mock
	ServiceDao servicedao;

	@Test

	@Order(1)
	public void testService() {

		ServiceBook servicerequest = new ServiceBook(101, 0, "tso5tv1212", "r15", "Yamaha", "nagol", "2 wheeler",
				"engine chek up", null, null, null, "nagol", "please chek engine cleary");
		when(servicedao.save(servicerequest)).thenReturn(servicerequest); // Mocking
		assertEquals(servicerequest,servicebookingservice.bookvehiclesservice(servicerequest));
		//assert (servicebookingservice.bookvehiclesservice(servicerequest).equals(servicerequest));
	}

	@Test

	@Order(2)
	public void testGetDetails() {

		ServiceBook servicestatus = new ServiceBook(101, 0, "tso5tv1212", "r15", "Yamaha", "nagol", "2 wheeler",
				"engine chek up", null, null, null, "nagol", "please chek engine cleary");
		String vehicleregistrationnumber = "tso5tv1212";
		when(servicedao.findByVehicleRegistrationNumber(vehicleregistrationnumber)).thenReturn(servicestatus);// Mocking
		assertEquals(servicestatus,servicebookingservice.getdetails(vehicleregistrationnumber));
		//assert (servicebookingservice.getdetails(vehicleregistrationnumber).equals(servicestatus));
	}

	@Test
	@Order(3)
	public void testUpdateService() {
		ServiceBook userdetailsupdate = new ServiceBook(101, 0, "tso5tv1212", "r15", "Yamaha", "nagol", "2 wheeler",
				"engine chek up", new Date(2022 - 1 - 11), new Date(2022 - 02 - 14), "kothapet", "nagol",
				"please chek engine cleary");
		String vehicleregistrationnumber = "tso5tv1212";
		String categery = "2 wheeler";
		int servicerequested_id = 101;
		String vehiclemodel = "r15";
		String vehiclename = "Yamaha";
		String service_branch = "nagol";
		String service_type = "engine chek up";
		String pickup_address = "kothapet";
		String delivery_address = "nagol";
		String vehicle_problem_desp = "please chek engine cleary";
		Date servicedate = new Date(2022 - 1 - 11);
		Date deliverydate = new Date(2022 - 02 - 14);
		when(servicedao.findByVehicleRegistrationNumber(vehicleregistrationnumber)).thenReturn(userdetailsupdate);
		when(servicedao.save(userdetailsupdate)).thenReturn(userdetailsupdate);// Mocking

		assertEquals(userdetailsupdate, servicebookingservice.update(userdetailsupdate, vehicleregistrationnumber));
		assertEquals(servicerequested_id,
				servicebookingservice.update(userdetailsupdate, vehicleregistrationnumber).getServicerequested_id());
		assertEquals(categery,
				servicebookingservice.update(userdetailsupdate, vehicleregistrationnumber).getCategiry());
		assertEquals(vehiclemodel,
				servicebookingservice.update(userdetailsupdate, vehicleregistrationnumber).getVehicle_modal());
		assertEquals(vehiclename,
				servicebookingservice.update(userdetailsupdate, vehicleregistrationnumber).getVehicle_name());
		assertEquals(service_branch,
				servicebookingservice.update(userdetailsupdate, vehicleregistrationnumber).getService_branch());
		assertEquals(service_type,
				servicebookingservice.update(userdetailsupdate, vehicleregistrationnumber).getService_type());
		assertEquals(pickup_address,
				servicebookingservice.update(userdetailsupdate, vehicleregistrationnumber).getPickup_address());
		assertEquals(delivery_address,
				servicebookingservice.update(userdetailsupdate, vehicleregistrationnumber).getDropping_address());
		assertEquals(vehicle_problem_desp, servicebookingservice.update(userdetailsupdate, vehicleregistrationnumber)
				.getVehicle_problem_description());
		assertEquals(servicedate,
				servicebookingservice.update(userdetailsupdate, vehicleregistrationnumber).getService_date());
		assertEquals(deliverydate,
				servicebookingservice.update(userdetailsupdate, vehicleregistrationnumber).getDelivery_date());
		assertEquals(vehicleregistrationnumber,
				servicebookingservice.update(userdetailsupdate, vehicleregistrationnumber).getVehicleRegistrationNumber());

		when(servicedao.findByVehicleRegistrationNumber(vehicleregistrationnumber)).thenReturn(null);
		assertThrows(DataNotFoundException.class, () -> {
			servicebookingservice.update(userdetailsupdate, vehicleregistrationnumber);
		});
	}
	@Test
	@Order(4)
	public void testCancelService() {
		ServiceBook servicecancel = new ServiceBook(101, 0, "tso5tv1212", "r15", "Yamaha", "nagol", "2 wheeler",
				"engine chek up", null, null, null, "nagol", "please chek engine cleary");
		String vehiclenumber = "tso5tv1212";
		when(servicedao.findByVehicleRegistrationNumber(vehiclenumber)).thenReturn(servicecancel);
		
			
		assertEquals(servicecancel ,servicebookingservice.cancelServiceRequest(vehiclenumber));
		verify(servicedao,
				  times(1)).delete(servicecancel); }
		//assertEquals(servicecancel, servicebookingservice.cancelServiceRequest(vehiclenumber));
	}

	

