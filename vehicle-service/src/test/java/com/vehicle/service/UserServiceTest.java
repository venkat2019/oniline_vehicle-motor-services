package com.vehicle.service;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.crypto.password.PasswordEncoder;

import com.vehicle.services.config.JwtTokenProvider;
import com.vehicle.services.model.User;
import com.vehicle.services.repository.UserRepositry;
import com.vehicle.services.service.UserService;

@SpringBootTest(classes = { UserServiceTest.class })
//@RunWith(SpringRunner.class)
//@WebMvcTest(UserService.class)
//@ExtendWith(MockitoExtension.class)
public class UserServiceTest {

	@Mock
	private UserRepositry userRepositry;

	@InjectMocks
	private UserService userService;

	@Mock
	public PasswordEncoder passwordEncoder;

	@Mock
	private AuthenticationManager authenticationManager;

	@Mock
	private JwtTokenProvider jwtTokenProvider;

	
	@Test

	@Order(3)
	public void test_findByUserName() {

		User user = new User(1, "indra", "indra123", "indra@gmail.com", "admin", null, null, null);

		String userName = "indra";
		when(userRepositry.findByUserName(userName)).thenReturn(user);
		assertEquals(user, userService.findByUserName(userName));
	}
	 
	@Test
	@Order(5)
	public void test_saveUser() {
		User user = new User(1, "indra", "indra@123", "indra@gmail.com", "admin", null, null, null);

		String password = passwordEncoder.encode("indra@123");

		when(userRepositry.findByEmailId(user.getEmailId())).thenReturn(null);
		when(passwordEncoder.encode(user.getPassword())).thenReturn(password);
		when(userRepositry.save(user)).thenReturn(user);
		//verify(userService, times(1)).saveUser(user);
		 userService.saveUser(user);

	}

	@Test
	@Order(5)
	public void test_signin() {

		String userName = "kamu";
		String password = "kamu";
		/*
		 * when(authenticationManager.authenticate(new
		 * UsernamePasswordAuthenticationToken(userName, password))) .thenReturn(null);
		 */
		String token = "qwertyu.456yhjkl.78uygbn";
		when(jwtTokenProvider.createToken(userName)).thenReturn(token);
		//assertEquals(token, userService.signin(userName, password));
		assertEquals("invalid password/username provided ", userService.signin(userName, password));
		//assertEquals(token, userService.signin(userName, password));
	}

}