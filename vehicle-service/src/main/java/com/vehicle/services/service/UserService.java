package com.vehicle.services.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.vehicle.services.config.JwtTokenProvider;
import com.vehicle.services.exceptions.UserAlreadyExistException;
import com.vehicle.services.model.User;
import com.vehicle.services.repository.UserRepositry;



@Service
public class UserService {

	@Autowired
	public UserRepositry userRepositry;

	

	@Autowired
	private JwtTokenProvider jwtTokenProvider;

	public UserService(UserRepositry userRepository) {
		this.userRepositry = userRepository;
	}

	// By using this user will be saved
	public void saveUser(User user) {
		if (emailExists(user.getEmailId())) {
			throw new UserAlreadyExistException("There is an account with that email address: " + user.getEmailId());
		}
		
		userRepositry.save(user);
	}

	// it will checks the entered email is already exist or not
	private boolean emailExists(final String emailId) {
		return userRepositry.findByEmailId(emailId) != null;
	}

	// this is used to a user by using user id
	/*
	 * public User getUserById(int userId) { return
	 * userRepositry.getUserById(userId); }
	 */
	public User findByUserName(String userName) {
		
		return userRepositry.findByUserName(userName);
	}


	public String signin(String userName, String password) {
		
		User exist = userRepositry.findByUserNameAndpassword(userName, password);
		
		if(exist !=null) {
			  return jwtTokenProvider.createToken(userName);
		  }else {
			  return "invalid password/username provided ";
		  }

	}


	/*
	 * public Boolean isUserExist(int id) { User user =
	 * userRepositry.findById(id).get(); if (user == null) return false; return
	 * true; }
	 */
	 
}
