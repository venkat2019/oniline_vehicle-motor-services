package com.vehicle.services.service;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.stereotype.Service;

import com.vehicle.services.exceptions.DataNotFoundException;
import com.vehicle.services.model.ServiceBook;
import com.vehicle.services.repository.ServiceDao;
import com.vehicle.services.serviceimpl.ServiceRequestServiceInterface;

@Service
public class ServiceBookingService implements ServiceRequestServiceInterface {

	@Autowired
	ServiceDao servicedao;

	@Override
	public ServiceBook bookvehiclesservice(ServiceBook servicerequest) {

		return servicedao.save(servicerequest);
		// return service;
	}

	@Override
	public ServiceBook getdetails(String vehicleregistrationnumber) {

		ServiceBook userExits = servicedao.findByVehicleRegistrationNumber(vehicleregistrationnumber);

		return userExits;

	}

	

	@Override
	public ServiceBook cancelServiceRequest(String vehicleregistrationnumber) {

		ServiceBook cancelrequest = servicedao.findByVehicleRegistrationNumber(vehicleregistrationnumber);
               
		   servicedao.delete(cancelrequest);
		
		return cancelrequest;

	}

	@Override
	public ServiceBook update(ServiceBook service, String vehicleregistrationnumber) {
		ServiceBook existinguser = servicedao.findByVehicleRegistrationNumber(vehicleregistrationnumber);

		if (existinguser != null) {
			existinguser.setDelivery_date(service.getDelivery_date());
			existinguser.setPickup_address(service.getPickup_address());

			existinguser.setCategiry(service.getCategiry());
			existinguser.setDropping_address(service.getDropping_address());
			existinguser.setService_branch(service.getService_branch());
			existinguser.setVehicle_modal(service.getVehicle_modal());
			existinguser.setVehicle_name(service.getVehicle_name());

			existinguser.setService_type(service.getService_type());

			servicedao.save(existinguser);
			//return "update succusessfully";

		} else {
			throw new DataNotFoundException(" user not found with id ");
		}

		return existinguser;
	}

}
