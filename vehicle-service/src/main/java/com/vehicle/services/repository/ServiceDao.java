package com.vehicle.services.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.vehicle.services.model.ServiceBook;

public interface ServiceDao extends JpaRepository<ServiceBook, Integer> {

	@Query("from ServiceBook where vehicleregistrationnumber=?1")
	ServiceBook findByVehicleRegistrationNumber(String vehicleregistrationnumber);

	
}
