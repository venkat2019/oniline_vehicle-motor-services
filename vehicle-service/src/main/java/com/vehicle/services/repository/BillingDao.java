package com.vehicle.services.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.vehicle.services.model.Billing;

public interface BillingDao extends JpaRepository<com.vehicle.services.model.Billing, Integer> {

	@Query(" from Billing where vehicle_registration_number =?1")
	Billing findById(String vehicle_registration_number);

}
