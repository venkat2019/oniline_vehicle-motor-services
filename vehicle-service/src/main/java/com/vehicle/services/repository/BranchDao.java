package com.vehicle.services.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.vehicle.services.model.Branches;

public interface BranchDao extends JpaRepository<Branches, Integer> {

	@Query("from Branches where branch =?1")
	Branches findBybranch(String branch);
}
