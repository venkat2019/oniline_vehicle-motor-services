package com.vehicle.services.repository;



import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.vehicle.services.model.User;

public interface UserRepositry extends JpaRepository<User, Integer> {

	@Query("from User where username =?1")
	User findByUserName(String userName);

	@Query("from User where username =?1 and password =?2")
	User findByUserNameAndpassword(String userName, String password);

	boolean existsByUserName(String userName);

	@Query("from User where email_id =?1")
	User findByEmailId(String emailId);

	@Query("from User where user_id = ?1")
	User getUserById(int userId);

}
