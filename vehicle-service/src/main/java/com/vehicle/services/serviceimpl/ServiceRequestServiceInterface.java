package com.vehicle.services.serviceimpl;



import com.vehicle.services.model.ServiceBook;

public interface ServiceRequestServiceInterface {

	ServiceBook getdetails(String vehicleregistrationnumber);

	//ServiceBook updateDetails(ServiceBook servicerequest1, String vehicleregistrationnumber);

	ServiceBook cancelServiceRequest(String vehicleregistrationnumber);

	ServiceBook bookvehiclesservice(ServiceBook servicerequest);
	
	ServiceBook update (ServiceBook service , String vehicleregistrationnumber);
}
