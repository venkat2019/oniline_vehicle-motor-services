package com.vehicle.services.enums;

public enum ServiceStatus {

	SERVICETAKEN,
	PROCESSING,
	COMPLETED
}
