package com.vehicle.services.controller;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.vehicle.services.model.User;
import com.vehicle.services.service.UserService;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
public class UserController {

	@Autowired
	public UserService userService;
	
	// user can register 

	@PostMapping("/Registration")
	@ApiOperation(value = "user have to register here", notes = "To register user has to provide their details", response = User.class)
	public String userRegistration( @RequestBody User user) {
		userService.saveUser(user);
		return "hi registered";
	}

	 // user can login here by entering username and password
	@ApiOperation(value = "Login User By UserName and Password ")
	@ApiResponses(value = {
			@ApiResponse(code = HttpServletResponse.SC_OK, response = String.class, message = "Logged in Successfully"),
			@ApiResponse(code = HttpServletResponse.SC_NOT_FOUND, response = String.class, message = "Invalid parameters") })
	@PostMapping("/login/{userName}/{password}")
	public ResponseEntity<String> loginByUserName(@PathVariable("userName") String userName,
			@PathVariable("password") String password) {
		String str = userService.signin(userName, password);
		if (!str.isEmpty()) {
			return new ResponseEntity<String>(str, HttpStatus.OK);
		}
		return new ResponseEntity<String>("token not generated", HttpStatus.NOT_FOUND);
	}

	// user can chek if user exist by that id or not 
	
}
