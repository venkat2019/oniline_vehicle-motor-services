package com.vehicle.services.controller;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.vehicle.services.exceptions.DataNotFoundException;
import com.vehicle.services.repository.BillingDao;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
@RequestMapping("/service-bill")
public class BillingController {
	
	@Autowired
	BillingDao billingdao;

	// user can chek service bill by enter vehicle registration number 
	@ApiOperation(value = "Get booking-details")
	@ApiResponses(value = {
			@ApiResponse(code = HttpServletResponse.SC_OK, response = String.class, message = "get the booking details successfully"),
			@ApiResponse(code = HttpServletResponse.SC_NOT_FOUND, response = String.class, message = "wrong parameters") ,
	        @ApiResponse(code = HttpServletResponse.SC_UNAUTHORIZED, response = String.class, message = "Invalid Token / Without Token"),
	        @ApiResponse(code = HttpServletResponse.SC_FORBIDDEN, response = String.class, message = "UnAuthorized Access") })

	@GetMapping("/billing/{vehiclenumber}")
	public com.vehicle.services.model.Billing fetching(@PathVariable("vehiclenumber") String vehicle_registration_number) {
		com.vehicle.services.model.Billing userbill= billingdao.findById(vehicle_registration_number);
          if(userbill == null ) {
        	  throw new DataNotFoundException("we have'nt generate the bill till now to this vehicle number ");
          }
		return userbill;
	}
}
