package com.vehicle.services.controller;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.vehicle.services.exceptions.DataNotFoundException;
import com.vehicle.services.model.Branches;
import com.vehicle.services.repository.BranchDao;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
@RequestMapping("/Branches")
public class Branch {

	/*
	 * @Autowired BranchProxy branchproxy;
	 */

	@Autowired
	BranchDao branchdao;

	// user can get the service center address & details

	@ApiOperation(value = "get the branch details ")

	@ApiResponses(value = {

			@ApiResponse(code = HttpServletResponse.SC_OK, response = String.class, message = "get branch address successfully"),
			@ApiResponse(code = HttpServletResponse.SC_NOT_FOUND, response = String.class, message = "wrong parameters"),
			@ApiResponse(code = HttpServletResponse.SC_UNAUTHORIZED, response = String.class, message = "Invalid Token / Without Token"),
			@ApiResponse(code = HttpServletResponse.SC_FORBIDDEN, response = String.class, message = "UnAuthorized Access") })

	@GetMapping("/branch-details/{branch_id}")
	public Branches getbranchaddress(@PathVariable("branch_id") String branch) {
	       Branches address = branchdao.findBybranch(branch);
	       if(address !=null) {
	    	   return address;
	       }else {
	    	   throw new DataNotFoundException("by this Name our branhces are not avaliable");
	       }

	}

}