package com.vehicle.services.controller;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.vehicle.services.exceptions.DataNotFoundException;
import com.vehicle.services.model.ServiceBook;
import com.vehicle.services.service.ServiceBookingService;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
@RequestMapping("/service")
public class Service {

	@Autowired
	ServiceBookingService servicebook;

	// user get the service booking form & fill it here for service 

	@ApiOperation(value = "booking vehicle service")
	@ApiResponses(value = {
			@ApiResponse(code = HttpServletResponse.SC_OK, response = String.class, message = "service booking successfully"),
			@ApiResponse(code = HttpServletResponse.SC_NOT_FOUND, response = String.class, message = "wrong parameters"),
	@ApiResponse(code = HttpServletResponse.SC_UNAUTHORIZED, response = String.class, message = "Invalid Token / Without Token"),
	@ApiResponse(code = HttpServletResponse.SC_FORBIDDEN, response = String.class, message = "UnAuthorized Access") })

	@PostMapping("/book")
	public String servicebooking(@RequestBody ServiceBook service) {
		servicebook.bookvehiclesservice(service);

		return "your vehicle service booking confim";
	}

	// user can get the their service booking details

	@ApiOperation(value = "Get booking-details")
	@ApiResponses(value = {
			@ApiResponse(code = HttpServletResponse.SC_OK, response = ServiceBook.class, message = "booking update successfully"),
			@ApiResponse(code = HttpServletResponse.SC_NOT_FOUND, response = ServiceBook.class, message = "wrong parameters"),
	@ApiResponse(code = HttpServletResponse.SC_UNAUTHORIZED, response = String.class, message = "Invalid Token / Without Token"),
	@ApiResponse(code = HttpServletResponse.SC_FORBIDDEN, response = String.class, message = "UnAuthorized Access") })

	@GetMapping("/status/{vehicle}")
	public ServiceBook getdetails(@PathVariable("vehicle") String vehicleregistrationnumber) {
		ServiceBook userExists = servicebook.getdetails(vehicleregistrationnumber);

		if (userExists == null) {
			throw new DataNotFoundException("user not found with this vehcile number to service  "
					+ vehicleregistrationnumber + " plese enter registered vehicle number ");
		}

		return userExists;

	}
	// user can update their vehicle service details

	
	
	
	@ApiOperation(value = "UPDATE DEATILS ")

	@ApiResponses(value = {

			@ApiResponse(code = HttpServletResponse.SC_OK, response = String.class, message = "Retrived successfully"),
			@ApiResponse(code = HttpServletResponse.SC_NOT_FOUND, response = String.class, message = "wrong parameters"),
	@ApiResponse(code = HttpServletResponse.SC_UNAUTHORIZED, response = String.class, message = "Invalid Token / Without Token"),
	@ApiResponse(code = HttpServletResponse.SC_FORBIDDEN, response = String.class, message = "UnAuthorized Access") })

	@PutMapping("/update/{vehiclenumber}")
	public String updatedeatilsById(@PathVariable("vehiclenumber") String vehicleregistrationnumber,

			@RequestBody ServiceBook service) {
		ServiceBook updating = servicebook.update(service, vehicleregistrationnumber);
		if (updating == null) {
			throw new DataNotFoundException(" please enter registered vehicle number  ");
		}

		return "updated successfully";
	}
	
	// user can cancel service request

	@ApiOperation(value = "cancel-ServiceRequest")
	@ApiResponses(value = {
			@ApiResponse(code = HttpServletResponse.SC_OK, response = String.class, message = "cancelled successfully"),
			@ApiResponse(code = HttpServletResponse.SC_NOT_FOUND, response = String.class, message = "wrong parameters"),
	@ApiResponse(code = HttpServletResponse.SC_UNAUTHORIZED, response = String.class, message = "Invalid Token / Without Token"),
	@ApiResponse(code = HttpServletResponse.SC_FORBIDDEN, response = String.class, message = "UnAuthorized Access") })

	@DeleteMapping("/cancel-request/{vehiclenumber}")
	public String cancelService(@PathVariable("vehiclenumber") String vehicleregistrationnumber) {

		ServiceBook cancelservice = servicebook.cancelServiceRequest(vehicleregistrationnumber);
		if (cancelservice == null) {
			throw new DataNotFoundException(" please enter vehicle register number  ");
		}

		return "you have canceled your service request";

	}

}
