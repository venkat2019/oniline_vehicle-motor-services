package com.vehicle.services.model;

import java.sql.Date;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;

import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.vehicle.services.enums.ServiceStatus;

@Entity
@Table(name = "service")
public class ServiceBook {

	@GeneratedValue
	private int servicerequested_id;
	@Id
	
	private String vehicleregistrationnumber;
	private String vehicle_modal;

	private String vehicle_name;

	
	private String service_branch;

	private String categiry;

	private String service_type;

	
	private Date service_date;

	private Date delivery_date;
   
	private String pickup_address;

    
	private String dropping_address;
    
   
	private String vehicle_problem_description;

	
	 @ManyToOne private User user ; 
	 

	@Enumerated(EnumType.STRING)
	private ServiceStatus status;

	public Date getService_date() {
		return service_date;
	}

	public void setService_date(Date service_date) {
		this.service_date = service_date;
	}

	public ServiceStatus getStatus() {
		return status;
	}

	public void setStatus(ServiceStatus status) {
		this.status = status;
	}

	public int getServicerequested_id() {
		return servicerequested_id;
	}

	public void setServicerequested_id(int servicerequested_id) {
		this.servicerequested_id = servicerequested_id;
	}

	public String getVehicleRegistrationNumber() {
		return vehicleregistrationnumber;
	}

	public void setVehicleRegistrationNumber(String vehicleRegistrationNumber) {
		vehicleregistrationnumber = vehicleRegistrationNumber;
	}

	public String getVehicle_modal() {
		return vehicle_modal;
	}

	public void setVehicle_modal(String vehicle_modal) {
		this.vehicle_modal = vehicle_modal;
	}

	public String getVehicle_name() {
		return vehicle_name;
	}

	public void setVehicle_name(String vehicle_name) {
		this.vehicle_name = vehicle_name;
	}

	public String getService_branch() {
		return service_branch;
	}

	public void setService_branch(String service_branch) {
		this.service_branch = service_branch;
	}

	public String getCategiry() {
		return categiry;
	}

	public void setCategiry(String categiry) {
		this.categiry = categiry;
	}

	public String getService_type() {
		return service_type;
	}

	public void setService_type(String service_type) {
		this.service_type = service_type;
	}

	public Date getSrvice_date() {
		return service_date;
	}

	public void setSrvice_date(Date srvice_date) {
		this.service_date = srvice_date;
	}

	public Date getDelivery_date() {
		return delivery_date;
	}

	public void setDelivery_date(Date delivery_date) {
		this.delivery_date = delivery_date;
	}

	public String getPickup_address() {
		return pickup_address;
	}

	public void setPickup_address(String pickup_address) {
		this.pickup_address = pickup_address;
	}

	public String getDropping_address() {
		return dropping_address;
	}

	public void setDropping_address(String dropping_address) {
		this.dropping_address = dropping_address;
	}

	public String getVehicle_problem_description() {
		return vehicle_problem_description;
	}

	public void setVehicle_problem_description(String vehicle_problem_description) {
		this.vehicle_problem_description = vehicle_problem_description;
	}

	public ServiceBook(int servicerequested_id, int user_id, String vehicleRegistrationNumber, String vehicle_modal,
			String vehicle_name, String service_branch, String categiry, String service_type, Date srvice_date,
			Date delivery_date, String pickup_address, String dropping_address, String vehicle_problem_description) {
		super();
		this.servicerequested_id = servicerequested_id;
		// this.user_id = user_id;
		vehicleregistrationnumber = vehicleRegistrationNumber;
		this.vehicle_modal = vehicle_modal;
		this.vehicle_name = vehicle_name;
		this.service_branch = service_branch;
		this.categiry = categiry;
		this.service_type = service_type;
		this.service_date = srvice_date;
		this.delivery_date = delivery_date;
		this.pickup_address = pickup_address;
		this.dropping_address = dropping_address;
		this.vehicle_problem_description = vehicle_problem_description;
	}

	public ServiceBook() {
		super();
	}

}
