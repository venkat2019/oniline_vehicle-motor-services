package com.vehicle.services.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;

import javax.persistence.Id;
import javax.persistence.Table;

import lombok.NonNull;

@Entity
@Table(name = "billing")
public class Billing {

	@GeneratedValue
	private int billing_id;
	private String vehicle_name;
	@Id
	private String vehicle_registration_number;
	private String parts_added;
	private int parts_price;

	private String branch_name;
	// private int servicerequested_id;

	private int Service_charge;

	private int delivery_charge;
	private int total_price;
	/*
	 * @NonNull private String payment_status;
	 */

	public String getParts_added() {
		return parts_added;
	}

	public void setParts_added(String parts_added) {
		this.parts_added = parts_added;
	}

	public int getParts_price() {
		return parts_price;
	}

	public void setParts_price(int parts_price) {
		this.parts_price = parts_price;
	}

	public String getBranch_name() {
		return branch_name;
	}

	public void setBranch_name(String branch_name) {
		this.branch_name = branch_name;
	}

	public int getBilling_id() {
		return billing_id;
	}

	public void setBilling_id(int billing_id) {
		this.billing_id = billing_id;
	}

	public String getVehicle_name() {
		return vehicle_name;
	}

	public void setVehicle_name(String vehicle_name) {
		this.vehicle_name = vehicle_name;
	}

	public @NonNull String getVehicle_registration_number() {
		return vehicle_registration_number;
	}

	public void setVehicle_registration_number(@NonNull String vehicle_registration_number) {
		this.vehicle_registration_number = vehicle_registration_number;
	}

	/*
	 * public int getServicerequested_id() { return servicerequested_id; } public
	 * void setServicerequested_id(int servicerequested_id) {
	 * this.servicerequested_id = servicerequested_id; }
	 */
	public int getService_charge() {
		return Service_charge;
	}

	public void setService_charge(int service_charge) {
		Service_charge = service_charge;
	}

	public int getDelivery_charge() {
		return delivery_charge;
	}

	public void setDelivery_charge(int delivery_charge) {
		this.delivery_charge = delivery_charge;
	}

	public int getTotal_price() {
		return total_price;
	}

	public void setTotal_price(int total_price) {
		this.total_price = total_price;
	}

	/*
	 * public String getPayment_status() { return payment_status; } public void
	 * setPayment_status(String payment_status) { this.payment_status =
	 * payment_status; }
	 */
	public Billing() {
		super();
	}

	public Billing(int billing_id, String vehicle_name, @NonNull String vehicle_registration_number, String parts_added,
			int parts_price, String branch_name, int service_charge, int delivery_charge, int total_price,
			String payment_status) {
		super();
		this.billing_id = billing_id;
		this.vehicle_name = vehicle_name;
		this.vehicle_registration_number = vehicle_registration_number;
		this.parts_added = parts_added;
		this.parts_price = parts_price;
		this.branch_name = branch_name;
		Service_charge = service_charge;
		this.delivery_charge = delivery_charge;
		this.total_price = total_price;
		// this.payment_status = payment_status;
	}

	@Override
	public String toString() {
		return "Billing [billing_id=" + billing_id + ", vehicle_name=" + vehicle_name + ", vehicle_registration_number="
				+ vehicle_registration_number + ", servicerequested_id=" + ", Service_charge=" + Service_charge
				+ ", delivery_charge=" + delivery_charge + ", total_price=" + total_price + "]";
	}

}
