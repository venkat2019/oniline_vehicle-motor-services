package com.services.vehicle.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.service.vehicle.model.User;
import com.service.vehicle.repository.UserDao;






@Service
public class MyUserDetails implements UserDetailsService {

  @Autowired
  private UserDao userRepository;

  @Override
  public UserDetails loadUserByUsername(String userNameOrmobileNumber) throws UsernameNotFoundException {
    final User user = userRepository.findByUserName(userNameOrmobileNumber);

    if (user == null) {
      throw new UsernameNotFoundException("User '" + userNameOrmobileNumber + "' not found");
    }

    return org.springframework.security.core.userdetails.User
        .withUsername(userNameOrmobileNumber)
        .password(user.getPassword())
        //.authorities(user.getRole())
        .accountExpired(false)
        .accountLocked(false)
        .credentialsExpired(false)
        .disabled(false)
        .build();
  }
  
  

}
