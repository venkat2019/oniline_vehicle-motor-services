package com.service.vehicle.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.NOT_FOUND)
public class DataNotFoundException extends RuntimeException{

	/**
	 * this exception used for if user 
	 * 
	 */
	private static final long serialVersionUID = 1L;
	

	  public DataNotFoundException (String messege) {
		  super(messege);
	  }
}
