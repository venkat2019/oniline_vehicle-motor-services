package com.service.vehicle.controller;

import java.sql.Date;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RestController;

import com.service.vehicle.exceptions.DataNotFoundException;
import com.service.vehicle.model.ServiceBook;
import com.service.vehicle.repository.ServiceDao;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
//@RequestMapping("/userorders")
public class BookingOrders {

	@Autowired
	ServiceDao servicedao;
	
	// admin can view the all booking service order 
	
	@ApiOperation(value = " service-branches")
	@ApiResponses(value = {
			@ApiResponse(code = HttpServletResponse.SC_OK, response = String.class, message = "view all booking orders"),
			@ApiResponse(code = HttpServletResponse.SC_NOT_FOUND, response = String.class, message = "wrong parameters"),
	@ApiResponse(code = HttpServletResponse.SC_UNAUTHORIZED, response = String.class, message = "Invalid Token / Without Token"),
	@ApiResponse(code = HttpServletResponse.SC_FORBIDDEN, response = String.class, message = "UnAuthorized Access") })

	@GetMapping("/service-orders")
	public List<ServiceBook> servicerequest() {
	//	return servicedao.findAll(orders);
		List<ServiceBook> userorders = servicedao.findAll();
		if (userorders != null) {
			
			return userorders;
		}else {
			throw new DataNotFoundException("till now no orders booking ");
		}
	}
	// admin will give servicing status 
	@ApiOperation(value = "admin will give the service update to user ")
	@ApiResponses(value = {
			@ApiResponse(code = HttpServletResponse.SC_OK, response = String.class, message = "status update succussfully"),
			@ApiResponse(code = HttpServletResponse.SC_NOT_FOUND, response = String.class, message = "wrong parameters"),
	@ApiResponse(code = HttpServletResponse.SC_UNAUTHORIZED, response = String.class, message = "Invalid Token / Without Token"),
	@ApiResponse(code = HttpServletResponse.SC_FORBIDDEN, response = String.class, message = "UnAuthorized Access") })

	@PutMapping("/service-update/{vehiclenumber}")
	public String status(@PathVariable ("vehiclenumber") String vehicleregistrationnumber , ServiceBook service) {
		ServiceBook status =servicedao.findbyVehicleNumber(vehicleregistrationnumber , service);
		
		if(status != null ) {
			status.setStatus(service.getStatus());
			servicedao.save(status);
			
			return "status update successfully";
			
		}else {
			return "please enter registered vehicle number ";
		}
	}
	// admin will easy fetching order details by enter service date 
	@ApiOperation(value = "admin will easy feteching  order details by service date ")
	@ApiResponses(value = {
			@ApiResponse(code = HttpServletResponse.SC_OK, response = String.class, message = "Retrived successfully"),
			@ApiResponse(code = HttpServletResponse.SC_NOT_FOUND, response = String.class, message = "wrong parameters"),
	@ApiResponse(code = HttpServletResponse.SC_UNAUTHORIZED, response = String.class, message = "Invalid Token / Without Token"),
	@ApiResponse(code = HttpServletResponse.SC_FORBIDDEN, response = String.class, message = "UnAuthorized Access") })

	@GetMapping("/orderByDate/{date}")
	public List<ServiceBook> orderbyDate(@PathVariable ("date") Date service_date ) {
		List<ServiceBook> service= servicedao.findByServiceDate(service_date);
		      
		  if(service==null) {
			  throw new DataNotFoundException("on this date no services booking");
		  }else {
			  return service;
		  }
		
				
	}
}
