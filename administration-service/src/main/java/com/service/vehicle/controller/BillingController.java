package com.service.vehicle.controller;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.service.vehicle.exceptions.DataNotFoundException;
//import com.service.vehicle.exceptions.DataNotFoundException;
//import com.admin.model.Billing;
import com.service.vehicle.service.BillingService;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
//@RequestMapping("/billing")
public class BillingController {

	@Autowired
	BillingService billingservice;

	// Admin will generate the bill for sending to users
	@ApiOperation(value = "user bill generated ")
	@ApiResponses(value = {
			@ApiResponse(code = HttpServletResponse.SC_OK, response = String.class, message = "bill generate successfully"),
			@ApiResponse(code = HttpServletResponse.SC_NOT_FOUND, response = String.class, message = "wrong parameters"),
	@ApiResponse(code = HttpServletResponse.SC_UNAUTHORIZED, response = String.class, message = "Invalid Token / Without Token"),
	@ApiResponse(code = HttpServletResponse.SC_FORBIDDEN, response = String.class, message = "UnAuthorized Access")})

	@PostMapping("/service-bill")
	public String servicebill(@RequestBody com.service.vehicle.model.Billing billgenerated) {
		billingservice.userbill(billgenerated);
		return "bill generated";
	}

	// Admin will check after completing vehicle service to user  bill generated or not
	

	@ApiOperation(value = "Get user booking-details")
	@ApiResponses(value = {
			@ApiResponse(code = HttpServletResponse.SC_OK, response = String.class, message = "Retrived successfully"),
			@ApiResponse(code = HttpServletResponse.SC_NOT_FOUND, response = String.class, message = "wrong parameters"),
	@ApiResponse(code = HttpServletResponse.SC_UNAUTHORIZED, response = String.class, message = "Invalid Token / Without Token"),
	@ApiResponse(code = HttpServletResponse.SC_FORBIDDEN, response = String.class, message = "UnAuthorized Access") })

	@GetMapping("/billing/{vehiclenumber}")
	public com.service.vehicle.model.Billing fetching(
			@PathVariable("vehiclenumber") String vehicle_registration_number) {
		com.service.vehicle.model.Billing userbill = billingservice.fetching(vehicle_registration_number);
		if (userbill == null) {
			throw new DataNotFoundException(
					"we have'nt generate the bill till now to this vehicle number or else plz enter registered vehicle number");
		}
		return userbill;
	}

	// Admin can update the billing for any new one

	@ApiOperation(value = "update-deatils")
	@ApiResponses(value = {
			@ApiResponse(code = HttpServletResponse.SC_OK, response = String.class, message = "Retrived successfully"),
			@ApiResponse(code = HttpServletResponse.SC_NOT_FOUND, response = String.class, message = "wrong parameters"),
	@ApiResponse(code = HttpServletResponse.SC_UNAUTHORIZED, response = String.class, message = "Invalid Token / Without Token"),
	@ApiResponse(code = HttpServletResponse.SC_FORBIDDEN, response = String.class, message = "UnAuthorized Access") })

	@PutMapping("/update/billing/{vehiclenumber}")
	public String updatedeatilsById(@PathVariable("vehiclenumber") String vehicleregistrationnumber,

			@RequestBody com.service.vehicle.model.Billing servicerequest1) {
		com.service.vehicle.model.Billing update = billingservice.update(servicerequest1, vehicleregistrationnumber);
		if (update == null) {
			throw new DataNotFoundException("please enter register vehicle number");
		}
		return "billing updated successfully";

	}

	// if admin want to delete billing

	@ApiOperation(value = "deleted-billing")
	@ApiResponses(value = {
			@ApiResponse(code = HttpServletResponse.SC_OK, response = String.class, message = "deleted billing successfully"),
			@ApiResponse(code = HttpServletResponse.SC_NOT_FOUND, response = String.class, message = "wrong parameters"),
	@ApiResponse(code = HttpServletResponse.SC_UNAUTHORIZED, response = String.class, message = "Invalid Token / Without Token"),
	@ApiResponse(code = HttpServletResponse.SC_FORBIDDEN, response = String.class, message = "UnAuthorized Access") })

	@DeleteMapping("/delete-billing/{vehiclenumber}")
	public String cancelServcie(@PathVariable("vehiclenumber") String vehicleregistrationnumber) {

		String cancel = billingservice.delete(vehicleregistrationnumber);
		if (cancel == null) {
			throw new DataNotFoundException("please enter register vehicle number");
		}
		return "billing was deleted by this vehicle number";

	}
}
