package com.service.vehicle.controller;

import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.service.vehicle.exceptions.DataNotFoundException;
import com.service.vehicle.model.User;
import com.service.vehicle.repository.UserDao;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

/**
 * User controller consists api's to retrive users
 */
@RestController
//@RequestMapping("/user")
public class UserController {

	@Autowired
	UserDao userdao;

	/*
	 * @Autowired private HeaderGenerator headerGenerator;
	 */
	/** This method is to View User By UserName
	 * @param userName
	 * @return User Instance
	 */
	
	
	/** View All Users
	 * @return List
	 */
	@ApiOperation(value = "View All Users")
	@ApiResponses(value = {
			@ApiResponse(code = HttpServletResponse.SC_OK, response = List.class, message = "View All Users"),
			@ApiResponse(code = HttpServletResponse.SC_NOT_FOUND, response = String.class, message = "Not Found"),
			@ApiResponse(code = HttpServletResponse.SC_UNAUTHORIZED, response = String.class, message = "Invalid Token / Without Token"),
			@ApiResponse(code = HttpServletResponse.SC_FORBIDDEN, response = String.class, message = "UnAuthorized Access") })

	@GetMapping("/users")
	public List<User> viewAllUser() {
		List<User> users = userdao.findAll();
		if (users !=null) {
			return users;
		}else {
		   throw new DataNotFoundException("till now users are not registered");
		}
		
	}
}
