package com.service.vehicle.controller;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.service.vehicle.exceptions.DataNotFoundException;
import com.service.vehicle.model.Materials;
import com.service.vehicle.service.MaterailService;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
//@RequestMapping("/store")
public class Materails {

	@Autowired
	MaterailService materailservice;

	// Admin can save all the materials

	@ApiOperation(value = "saving the stock  ")
	@ApiResponses(value = {
			@ApiResponse(code = HttpServletResponse.SC_OK, response = String.class, message = "save materails succussfully "),
			@ApiResponse(code = HttpServletResponse.SC_NOT_FOUND, response = String.class, message = "wrong parameters"),
	@ApiResponse(code = HttpServletResponse.SC_UNAUTHORIZED, response = String.class, message = "Invalid Token / Without Token"),
	@ApiResponse(code = HttpServletResponse.SC_FORBIDDEN, response = String.class, message = "UnAuthorized Access") })

	@PostMapping("/save-materials")
	public Materials store(@RequestBody Materials showroommaterails) {
		return materailservice.storematerials(showroommaterails);
	}
	@ApiOperation(value = "chek available stock ")
	@ApiResponses(value = {
			@ApiResponse(code = HttpServletResponse.SC_OK, response = String.class, message = "view all stocks"),
			@ApiResponse(code = HttpServletResponse.SC_NOT_FOUND, response = String.class, message = "wrong parameters"),
	@ApiResponse(code = HttpServletResponse.SC_UNAUTHORIZED, response = String.class, message = "Invalid Token / Without Token"),
	@ApiResponse(code = HttpServletResponse.SC_FORBIDDEN, response = String.class, message = "UnAuthorized Access") })

	@GetMapping("/materails-/{branch_id}")
	public Materials store(@PathVariable("branch_id") int branch_id ) {
	
		Materials materail = materailservice.getmaterails(branch_id);
		if (materail != null) {
			
			return materail;
		}else {
			throw new DataNotFoundException("Plz enter valid branch id");
		}
	}

	// update the materails
	@ApiOperation(value = "update stock if any  ")
	@ApiResponses(value = {
			@ApiResponse(code = HttpServletResponse.SC_OK, response = String.class, message = "update stock successfully"),
			@ApiResponse(code = HttpServletResponse.SC_NOT_FOUND, response = String.class, message = "wrong parameters"),
	@ApiResponse(code = HttpServletResponse.SC_UNAUTHORIZED, response = String.class, message = "Invalid Token / Without Token"),
	@ApiResponse(code = HttpServletResponse.SC_FORBIDDEN, response = String.class, message = "UnAuthorized Access") })

	@PutMapping("/products-update/{branchid}")
	public String materailUpdate(@PathVariable("branchid") int branch_id, Materials update) {
		Materials branchid = materailservice.updateDetails(update, branch_id);

		if (branchid == null) {
			throw new DataNotFoundException("plz enter available branch id");
		}
		return "stock updated";
	}

}
