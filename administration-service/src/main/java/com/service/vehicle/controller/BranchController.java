package com.service.vehicle.controller;



import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.service.vehicle.exceptions.DataNotFoundException;
import com.service.vehicle.service.Branch;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
//@RequestMapping("/branches")
public class BranchController {

	@Autowired
	Branch branch;

	// admin can give branch details and address

	@ApiOperation(value = " service-branches")
	@ApiResponses(value = {
			@ApiResponse(code = HttpServletResponse.SC_OK, response = BranchController.class, message = "Retrived successfully"),
			@ApiResponse(code = HttpServletResponse.SC_NOT_FOUND, response = BranchController.class, message = "wrong parameters"),
	@ApiResponse(code = HttpServletResponse.SC_UNAUTHORIZED, response = String.class, message = "Invalid Token / Without Token"),
	@ApiResponse(code = HttpServletResponse.SC_FORBIDDEN, response = String.class, message = "UnAuthorized Access") })

	@PostMapping("/save-branch/details")
	public com.service.vehicle.model.Branches address(@RequestBody com.service.vehicle.model.Branches address) {
		return branch.providied(address);
	}

	 // Get the branch details admin will chek it which details are here 
	@ApiOperation(value = "branch details ")
	@ApiResponses(value = {
			@ApiResponse(code = HttpServletResponse.SC_OK, response = String.class, message = "Retrived successfully"),
			@ApiResponse(code = HttpServletResponse.SC_NOT_FOUND, response = String.class, message = "wrong parameters"),
	@ApiResponse(code = HttpServletResponse.SC_UNAUTHORIZED, response = String.class, message = "Invalid Token / Without Token"),
	@ApiResponse(code = HttpServletResponse.SC_FORBIDDEN, response = String.class, message = "UnAuthorized Access") })

	@GetMapping("/branch-details/{branch_id}")
	public com.service.vehicle.model.Branches servicerequest(@PathVariable("branch_id") int branch_id ) {
	//	return servicedao.findAll(orders);
		com.service.vehicle.model.Branches details = branch.getbranch(branch_id);
		if (details != null) {
			
			return details;
		}else {
			throw new DataNotFoundException("address details are not found by that branch id plz enter register branch id ");
		}
	}
	// branch location & contact details update
	
	@ApiOperation(value = " update branch details ")
	@ApiResponses(value = {
			@ApiResponse(code = HttpServletResponse.SC_OK, response = String.class, message = "Retrived successfully"),
			@ApiResponse(code = HttpServletResponse.SC_NOT_FOUND, response = String.class, message = "wrong parameters"),
	@ApiResponse(code = HttpServletResponse.SC_UNAUTHORIZED, response = String.class, message = "Invalid Token / Without Token"),
	@ApiResponse(code = HttpServletResponse.SC_FORBIDDEN, response = String.class, message = "UnAuthorized Access") })

	@PutMapping("/details-update/{branchid}")
	public com.service.vehicle.model.Branches addressUpdate(@PathVariable("branchid") int branch_id,
			com.service.vehicle.model.Branches update) {
		com.service.vehicle.model.Branches branchDetailsUpdate = branch.updateDetails(update, branch_id);

		if (branchDetailsUpdate == null) {
			throw new DataNotFoundException("plz enter available branch id ");
		}
		return branchDetailsUpdate;
	}
}