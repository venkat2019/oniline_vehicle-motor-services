package com.service.vehicle.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.service.vehicle.model.Branches;

public interface BranchDao extends JpaRepository<Branches, Integer> {

	
	  @Query("from Branches where branch_id=?1") Branches findById(int branch_id);
	 
}
