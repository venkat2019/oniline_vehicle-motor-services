package com.service.vehicle.repository;

import java.sql.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.service.vehicle.model.ServiceBook;

public interface ServiceDao extends JpaRepository<ServiceBook, Integer> {

	@Query("from ServiceBook")
	List<ServiceBook> findAll(ServiceBook servicerequest);

	@Query("from ServiceBook where service_date =?1")
	List<ServiceBook> findByServiceDate(Date service_date);

	@Query("from ServiceBook where vehicleregistrationnumber =?1")
	ServiceBook findbyVehicleNumber(String vehicleregistrationnumber, ServiceBook service);
}
