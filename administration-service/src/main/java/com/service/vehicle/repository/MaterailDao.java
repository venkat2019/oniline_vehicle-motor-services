package com.service.vehicle.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

//import com.service.vehicle.model.Branches;
import com.service.vehicle.model.Materials;

public interface MaterailDao extends JpaRepository<Materials, Integer> {

	@Query("from Materials where branch_id=?1")
	Materials findById(int branch_id);

}
