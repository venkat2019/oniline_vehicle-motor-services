package com.service.vehicle.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.service.vehicle.model.User;

/**
 * User Repository Interface extends JpaRepository
 * 
 * @author saipavan
 */
@Repository
public interface UserDao extends JpaRepository<User, Long> {

	/**
	 * This method fetches user byuserName
	 * 
	 * @param userName
	 * @return User Instance
	 */

	@Query("from User")
	List<User> findAll(User userdetails);

	User findByUserName(String userName);

}
