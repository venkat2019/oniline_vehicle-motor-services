package com.service.vehicle.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.service.vehicle.model.Billing;

public interface BillingDao extends JpaRepository<Billing, Integer> {

	
	
	@Query(" from Billing where vehicle_registration_number =?1")
	Billing findById(String vehicle_registration_number);

}
