package com.service.vehicle.model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "materials")
public class Materials {
	@Id
	private int branch_id;
	private String Tyrebrand;
	private int tyrebrand_quantity;
	private String engine_oil;
	private int engine_oil_quantity;

	public int getBranch_id() {
		return branch_id;
	}

	public void setBranch_id(int branch_id) {
		this.branch_id = branch_id;
	}

	public String getTyrebrand() {
		return Tyrebrand;
	}

	public void setTyrebrand(String tyrebrand) {
		Tyrebrand = tyrebrand;
	}

	public int getTyrebrand_quantity() {
		return tyrebrand_quantity;
	}

	public void setTyrebrand_quantity(int tyrebrand_quantity) {
		this.tyrebrand_quantity = tyrebrand_quantity;
	}

	public String getEngine_oil() {
		return engine_oil;
	}

	public void setEngine_oil(String engine_oil) {
		this.engine_oil = engine_oil;
	}

	public int getEngine_oil_quantity() {
		return engine_oil_quantity;
	}

	public void setEngine_oil_quantity(int engine_oil_quantity) {
		this.engine_oil_quantity = engine_oil_quantity;
	}

	public Materials() {
		super();
	}

	public Materials(int branch_id, String tyrebrand, int tyrebrand_quantity, String engine_oil,
			int engine_oil_quantity) {
		super();
		this.branch_id = branch_id;
		Tyrebrand = tyrebrand;
		this.tyrebrand_quantity = tyrebrand_quantity;
		this.engine_oil = engine_oil;
		this.engine_oil_quantity = engine_oil_quantity;
	}

	@Override
	public String toString() {
		return "Materials [branch_id=" + branch_id + ", Tyrebrand=" + Tyrebrand + ", tyrebrand_quantity="
				+ tyrebrand_quantity + ", engine_oil=" + engine_oil + ", engine_oil_quantity=" + engine_oil_quantity
				+ "]";
	}

}
