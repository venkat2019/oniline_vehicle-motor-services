package com.service.vehicle.model;

import javax.persistence.Entity;

import javax.persistence.Id;
import javax.persistence.Table;


@Entity
@Table(name = "Branches")
public class Branches {

	
	@Id
	
	private int branch_id;
	private String branch;
	private String address;
	private int contact_number;
	public int getBranch_id() {
		return branch_id;
	}
	public void setBranch_id(int branch_id) {
		this.branch_id = branch_id;
	}
	public String getBranch() {
		return branch;
	}
	public void setBranch(String branch) {
		this.branch = branch;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public int getContact_number() {
		return contact_number;
	}
	public void setContact_number(int contact_number) {
		this.contact_number = contact_number;
	}
	public Branches() {
		super();
	}
	public Branches(int branch_id, String branch, String address, int contact_number) {
		super();
		this.branch_id = branch_id;
		this.branch = branch;
		this.address = address;
		this.contact_number = contact_number;
	}
	@Override
	public String toString() {
		return "Serivce_branches [branch_id=" + branch_id + ", branch=" + branch + ", address=" + address
				+ ", contact_number=" + contact_number + "]";
	}
	
}
