package com.service.vehicle.model;



import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import javax.persistence.Table;





@Entity
@Table(name = "Registration")

public class User {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int user_id;

	
	@Column(name = "username")

	private String userName;

	@Column(name = "first_name")

	private String firstName;

	@Column(name = "last_name")

	private String lastName;
    
	@Column(name = "email_id")

	private String emailId;

	
	@Column(name = "mobile_number")

	private String mobileNumber;

	
	@Column(name = "password")

	private String password;
	
	/*
	 * @OneToMany
	 * 
	 * @JsonIgnore private List<ServiceBook> serivceorders;
	 */

	public int getId() {
		return user_id;
	}

	public User(String emailId, String password) {
		super();
		this.emailId = emailId;
		this.password = password;
	}

	public User() {
		super();
	}

	public User(int id, String userName, String firstName, String lastName, String emailId, String mobileNumber,
			String password) {
		super();
		this.user_id = id;
		this.userName = userName;
		this.firstName = firstName;
		this.lastName = lastName;
		this.emailId = emailId;
		this.mobileNumber = mobileNumber;
		this.password = password;
	}

	public void setId(int id) {
		this.user_id = id;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String username) {
		this.userName = username;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getEmailId() {
		return emailId;
	}

	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	public String getMobileNumber() {
		return mobileNumber;
	}

	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

}
