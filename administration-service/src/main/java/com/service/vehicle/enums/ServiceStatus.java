package com.service.vehicle.enums;


public enum ServiceStatus {

	SERVICETAKEN,
	PROCESSING,
	COMPLETED
}
