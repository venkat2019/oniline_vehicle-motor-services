package com.service.vehicle.service;



import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.service.vehicle.exceptions.DataNotFoundException;
import com.service.vehicle.impl.MaterailInterface;

import com.service.vehicle.model.Materials;
import com.service.vehicle.repository.MaterailDao;

@Service
public class MaterailService implements MaterailInterface {

	@Autowired
	MaterailDao materaildao;

	@Override
	public Materials storematerials(Materials materails) {

		return materaildao.save(materails);
	}

	@Override
	public Materials updateDetails(Materials materailsupdate, int branch_id) {
		Materials branchid = materaildao.findById(branch_id);

		if (branchid != null) {
			branchid.setEngine_oil(materailsupdate.getEngine_oil());
			branchid.setEngine_oil_quantity(materailsupdate.getEngine_oil_quantity());
			branchid.setTyrebrand(materailsupdate.getTyrebrand());
			branchid.setTyrebrand_quantity(materailsupdate.getTyrebrand_quantity());
			materaildao.save(branchid);
		} else {
			throw new DataNotFoundException("by this brnch id no branches avalibale ");
		}
		return branchid;
	}

	@Override
	public Materials getmaterails(int branch_id) {
		
		return materaildao.findById(branch_id);
	}

}
