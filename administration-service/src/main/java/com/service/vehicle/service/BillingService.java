package com.service.vehicle.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.service.vehicle.exceptions.DataNotFoundException;
import com.service.vehicle.impl.BillingInterface;
import com.service.vehicle.model.Billing;
import com.service.vehicle.repository.BillingDao;

@Service
public class BillingService implements BillingInterface {

	@Autowired
	BillingDao billingdao;

	@Override
	public Billing userbill(Billing servicerequest) {

		return billingdao.save(servicerequest);
	}

	@Override
	public Billing fetching(String vehicle_registration_number) {

		return billingdao.findById(vehicle_registration_number);

	}

	@Override
	public Billing update(Billing servicerequest1, String vehicle_registration_number) {

		Billing existinguser = billingdao.findById(vehicle_registration_number);

		if (existinguser != null) {
			existinguser.setDelivery_charge(servicerequest1.getDelivery_charge());
			existinguser.setBranch_name(servicerequest1.getBranch_name());
			existinguser.setTotal_price(servicerequest1.getTotal_price());
			existinguser.setParts_added(servicerequest1.getParts_added());
			existinguser.setParts_price(servicerequest1.getParts_price());
			existinguser.setService_charge(servicerequest1.getService_charge());
			existinguser.setVehicle_name(servicerequest1.getVehicle_name());
			existinguser.setVehicle_registration_number(servicerequest1.getVehicle_registration_number());
			billingdao.save(existinguser);

		} else {
			throw new DataNotFoundException(" user not found with id ");
		}

		return existinguser;

	}

	@Override
	public String delete(String vehicleregistrationnumber) {

		Billing delatebill = billingdao.findById(vehicleregistrationnumber);

		billingdao.delete(delatebill);
		return "deleted";
	}

}
