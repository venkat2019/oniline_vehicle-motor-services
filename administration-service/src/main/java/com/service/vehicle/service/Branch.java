package com.service.vehicle.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.service.vehicle.exceptions.DataNotFoundException;
import com.service.vehicle.impl.BranchInterface;
import com.service.vehicle.model.Branches;
import com.service.vehicle.repository.BranchDao;

@Service
public class Branch implements BranchInterface {

	@Autowired
	BranchDao branchdao;

	@Override
	public Branches providied(Branches providied) {

		return branchdao.save(providied);
	}

	@Override
	public Branches updateDetails(Branches detialsupdate, int branch_id) {

		Branches branch = branchdao.findById(branch_id);
		if (branch != null) {
			branch.setAddress(detialsupdate.getAddress());
			branch.setBranch(detialsupdate.getBranch());
			branch.setContact_number(detialsupdate.getContact_number());
			branchdao.save(branch);
		} else {
			throw new DataNotFoundException("by this branch id no branches available ");
		}
		return branch;
	}

	@Override
	public Branches getbranch(int branch_id) {
		
		return branchdao.findById(branch_id);
	}
}
