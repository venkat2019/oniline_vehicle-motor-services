package com.service.vehicle.impl;

import com.service.vehicle.model.Billing;

public interface BillingInterface {

	Billing userbill(Billing userBill);

	Billing update(Billing userbills, String vehicleregistrationnumber);

	Billing fetching(String vehicleregistrationnumber);
	
	String delete (String vehicleregistrationnumber);
	
}
