package com.service.vehicle;


import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.service.vehicle.model.Billing;
import com.service.vehicle.repository.BillingDao;
import com.service.vehicle.service.BillingService;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = { BillingTest.class })
public class BillingTest {

	@Mock
	BillingDao billingdao;

	@InjectMocks
	BillingService billingservice;

	/**
	 * if this test class specified in the service class billing details saving
	 * class is working or not
	 */
	@Test
	public void testSaveBiiling() {
		Billing userbilling = new Billing(101, "fz", "ts78tv1234", "dume", 320, "uppal", 30, 40, 650, null);
		when(billingdao.save(userbilling)).thenReturn(userbilling);// Mocking
		assertEquals(userbilling, billingservice.userbill(userbilling));
	}

	@Test
	@Order(2)
	public void testGetBillingDetails() {

		Billing userdetails = new Billing(101, "fz", "ts78tv1234", "dume", 320, "uppal", 30, 40, 650, null);
		String vehicleregistrationnumber = "ts78tv1234";
		when(billingdao.findById(vehicleregistrationnumber)).thenReturn(userdetails);// Mocking
		assertEquals(userdetails,billingservice.fetching(vehicleregistrationnumber));
	}

	@Test
	@Order(3)
	public void testUpdateBilling() {
		Billing userbillupdate = new Billing(101, "fz", "ts78tv1234", "dume", 320, "uppal", 30, 40, 650, null);
		String vehilcenumber = "ts78tv1234";
		int billing_id = 101;
		String vehicle_name = "fz";
		String partsadded = "dume";
		int parts_price = 320;
		String branch_name = "uppal";
		int service_charge = 30;
		int delivery_charge = 40;
		int total_price = 650;

		when(billingdao.findById(vehilcenumber)).thenReturn(userbillupdate);// Mocking
		when(billingdao.save(userbillupdate)).thenReturn(userbillupdate);// Mocking
		assertEquals(userbillupdate, billingservice.update(userbillupdate, vehilcenumber));
		assertEquals(billing_id, billingservice.update(userbillupdate, vehilcenumber).getBilling_id());
		assertEquals(vehicle_name, billingservice.update(userbillupdate, vehilcenumber).getVehicle_name());
		assertEquals(partsadded, billingservice.update(userbillupdate, vehilcenumber).getParts_added());
		assertEquals(parts_price, billingservice.update(userbillupdate, vehilcenumber).getParts_price());
		assertEquals(branch_name, billingservice.update(userbillupdate, vehilcenumber).getBranch_name());
		assertEquals(delivery_charge, billingservice.update(userbillupdate, vehilcenumber).getDelivery_charge());
		assertEquals(service_charge, billingservice.update(userbillupdate, vehilcenumber).getService_charge());
		assertEquals(total_price, billingservice.update(userbillupdate, vehilcenumber).getTotal_price());
		assertEquals(vehilcenumber, billingservice.update(userbillupdate, vehilcenumber).getVehicle_registration_number());
		
	}

	@Test
	@Order(4)
	public void testCancelBill() {
		Billing userbillcancel = new Billing(101, "ts78tv1234", "fz", "no", 320, "uppal", 30, 0, 650, null);
		String vehilcenumber = "ts78tv1234";
          when(billingdao.findById(vehilcenumber)).thenReturn(userbillcancel);
          assertEquals("deleted", billingservice.delete(vehilcenumber));
          
          //assertEquals(servicecancel ,servicebookingservice.cancelServiceRequest(vehiclenumber));
  		verify(billingdao,
  				  times(1)).delete(userbillcancel); }
	}
	

