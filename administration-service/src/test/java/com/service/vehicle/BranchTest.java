package com.service.vehicle;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThrows;
import static org.mockito.Mockito.when;

import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.service.vehicle.exceptions.DataNotFoundException;
import com.service.vehicle.model.Branches;
import com.service.vehicle.repository.BranchDao;
import com.service.vehicle.service.Branch;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = { BranchTest.class })
public class BranchTest {

	@Mock
	BranchDao branchdao;

	@InjectMocks
	Branch branchservice;

	@Test
	public void testSaveBranch() {
		Branches branch = new Branches(103, "kothapet", "11-8-92/4, nagol", 818685141);
		when(branchdao.save(branch)).thenReturn(branch);// Mocking
		assertEquals(branch, branchservice.providied(branch));
	}

	@Test
	@Order(2)
	public void testGetBranchDetails() {

		Branches branchdetails = new Branches(101, "nagol", "11-8-2/4 uppal", 871231753);
		int branch_id = 101;
		when(branchdao.findById(branch_id)).thenReturn(branchdetails);// Mocking
		assertEquals(branchdetails ,branchservice.getbranch(branch_id));
	}

	@Test
	@Order(3)
	public void testUpdateBranch() {
		Branches updatebranchdetails = new Branches(101, "nagol", "11-8-2/4 uppal", 871231753);
		int branch_id = 101;
		String branch = "nagol";
		String address = "11-8-2/4 uppal";
		int contact_number = 871231753;
		when(branchdao.findById(branch_id)).thenReturn(updatebranchdetails);// Mocking
		when(branchdao.save(updatebranchdetails)).thenReturn(updatebranchdetails);// Mocking
		assertEquals(updatebranchdetails, branchservice.updateDetails(updatebranchdetails, branch_id));
		assertEquals(address, branchservice.updateDetails(updatebranchdetails, branch_id).getAddress());
		assertEquals(branch_id, branchservice.updateDetails(updatebranchdetails, branch_id).getBranch_id());
		assertEquals(branch, branchservice.updateDetails(updatebranchdetails, branch_id).getBranch());
		assertEquals(contact_number, branchservice.updateDetails(updatebranchdetails, branch_id).getContact_number());
		when(branchdao.findById(branch_id)).thenReturn(null);
		assertThrows(DataNotFoundException.class,()->{branchservice.updateDetails(updatebranchdetails, branch_id);});
	}

}
