package com.service.vehicle.controller;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThrows;
import static org.mockito.Mockito.when;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;

import com.service.vehicle.exceptions.DataNotFoundException;
import com.service.vehicle.model.ServiceBook;
import com.service.vehicle.repository.ServiceDao;

@SpringBootTest(classes = { BookingOrdersTest.class })
public class BookingOrdersTest {

	@Mock
	ServiceDao serviceDao;
	
	
	@InjectMocks
	BookingOrders bookingorders;

	@Test
	@Order(1)
	public void testBookingOrders() {

		List<ServiceBook> userorders = new ArrayList<ServiceBook>();
		userorders.add(new ServiceBook(1, "yamaha", "ts09tv1890", "nagol", "kothapet", "2021-09-12", "2021-10-1", null,
				null, null, "kothapet", "plase chek engine cleary"));
		userorders.add(new ServiceBook(1, "yamaha", "ts09tv1890", "nagol", "kothapet", "2021-09-12", "2021-10-1", null,
				null, null, "kothapet", "plase chek engine cleary"));

		when(serviceDao.findAll()).thenReturn(userorders);
		assertEquals(userorders.size(), bookingorders.servicerequest().size());
		when(serviceDao.findAll()).thenReturn(null);
		assertThrows(DataNotFoundException.class,()->{bookingorders.servicerequest();});
	}

	@Test
	@Order(2)
	public void testStatusUpdateByVehicleNumber() {

		ServiceBook user = new ServiceBook(1, "yamaha", "ts09tv1890", "nagol", "kothapet", "2021-09-12", "2021-10-1",
				null, new Date(2020 - 8 - 8), null, null, null);
		// String status = "completed";
		String vehilcenumber = "ts78tv1234";
		String number1 = "tes78tv16";
		when(serviceDao.findbyVehicleNumber(vehilcenumber, user)).thenReturn(user);// Mocking
		when(serviceDao.save(user)).thenReturn(user);
		when(serviceDao.findbyVehicleNumber(number1, user)).thenReturn(user);

		assertEquals("status update successfully", bookingorders.status(vehilcenumber, user));

		when(serviceDao.findbyVehicleNumber(vehilcenumber, user)).thenReturn(null);
		assertThrows(DataNotFoundException.class,()->{bookingorders.status(vehilcenumber, user);});
		
	}

	@Test
	@Order(3)
	public void testBookingOrderByDate() {

		List<ServiceBook> userorders = new ArrayList<ServiceBook>();
		userorders.add(new ServiceBook(1, "yamaha", "ts09tv1890", "nagol", "kothapet", "2021-09-12", "2021-10-1", new Date(2012 - 9 - 12),
				new Date(2012 - 9 - 12), null, "kothapet", "plase chek engine cleary"));
		userorders.add(new ServiceBook(1, "yamaha", "ts09tv1890", "nagol", "kothapet", "2021-09-12", "2021-10-1", null,
				null, null, "kothapet", "plase chek engine cleary"));
		
		Date servicedate = new Date(2012 - 9 - 12);
		when(serviceDao.findByServiceDate(servicedate)).thenReturn(userorders);
		assertEquals(userorders.size(), bookingorders.orderbyDate(servicedate).size());
		when(serviceDao.findByServiceDate(servicedate)).thenReturn(null);
		assertThrows(DataNotFoundException.class,()->{bookingorders.orderbyDate(servicedate);});
		
	}

}
