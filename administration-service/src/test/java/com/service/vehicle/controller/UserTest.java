package com.service.vehicle.controller;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThrows;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;

import com.service.vehicle.exceptions.DataNotFoundException;
import com.service.vehicle.model.User;
import com.service.vehicle.repository.UserDao;

@SpringBootTest(classes = {UserTest.class})
public class UserTest {

	@Mock
	UserDao userdao;
	
	@InjectMocks
	UserController usercontroller;
	
	/** user controller test class  */
	@Test
	public void testUser() {
		List<User> users = new ArrayList<User>();
		users.add(new User(1 , "venkatreddy" , "venkat" , "reddy", "@gmail.com", "ravi", null));
		users.add(new User(1 , "venkatreddy" , "venkat" , "reddy", null, null, null));
		when(userdao.findAll()).thenReturn(users);
		assertEquals(users.size(), usercontroller.viewAllUser().size());
		//assertEquals("till now users are not registered", DataNotFoundException.class);
		when(userdao.findAll()).thenReturn(null);
		assertThrows(DataNotFoundException.class,()->{usercontroller.viewAllUser();});
		
	}
}

