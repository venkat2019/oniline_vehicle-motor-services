package com.service.vehicle.controller;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThrows;
import static org.mockito.Mockito.when;

import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;

import com.service.vehicle.exceptions.DataNotFoundException;
import com.service.vehicle.model.Billing;
import com.service.vehicle.service.BillingService;

@SpringBootTest(classes = { BillingTest.class })
public class BillingTest {

	@Mock
	BillingService billingservice;

	@InjectMocks
	BillingController billing;

	@Test
	@Order(1)
	public void testSaveBill() {

		Billing billgenerated = new Billing(103, "ts07tv1990",
				"yamaha", "dume", 320, "nagol", 40, 320, 9, null);

		when(billingservice.userbill(billgenerated)).thenReturn(billgenerated);// Mocking
		assertEquals("bill generated", billing.servicebill(billgenerated));

	}
	@Test
	@Order(2)
	public void testGetBillingDetails() {

		Billing billgenerated = new Billing(101, "ts78tv1234", "fz", "no", 320, "uppal", 30, 0, 650, null);
		String vehicleregistrationnumber = "ts78tv1234";
		when(billingservice.fetching(vehicleregistrationnumber)).thenReturn(billgenerated);// Mocking
		assertEquals(billgenerated, billing.fetching(vehicleregistrationnumber));
		when(billingservice.fetching(vehicleregistrationnumber)).thenReturn(null);
		assertThrows(DataNotFoundException.class,()->{billing.fetching(vehicleregistrationnumber);});
	}
	
	@Test
	@Order(3)
	public void testUpdateBilling() {
		Billing userbillupdate = new Billing(101, "ts78tv1234", "fz", "no", 320, "uppal", 30, 0, 650, null);
		String vehilcenumber = "ts78tv1234";
		
		when(billingservice.update(userbillupdate , vehilcenumber)).thenReturn(userbillupdate);// Mocking
		assertEquals("billing updated successfully", billing.updatedeatilsById(vehilcenumber, userbillupdate));
		/*
		 * assertEquals(billing_id, billing.updatedeatilsById( billing_id
		 * ,userbillupdate).()); assertEquals(pass,
		 * busservice.changeMemberDetails(memberid, member).getPassword());
		 */
		when(billingservice.update(userbillupdate , vehilcenumber)).thenReturn(null);
		assertThrows(DataNotFoundException.class,()->{billing.updatedeatilsById(vehilcenumber, userbillupdate);});
	}
	@Test
	@Order(4)
	public void testCancelBill() {
		Billing userbillcancel = new Billing(101, "ts78tv1234", "fz", "no", 320, "uppal", 30, 0, 650, null);
		String vehilcenumber = "ts78tv1234";
          when(billingservice.delete(vehilcenumber)).thenReturn(vehilcenumber);
          assertEquals("billing was deleted by this vehicle number", billing.cancelServcie(vehilcenumber));
          when(billingservice.delete(vehilcenumber)).thenReturn(null);
          assertThrows(DataNotFoundException.class,()->{billing.cancelServcie(vehilcenumber);});
	}
}

