package com.service.vehicle.controller;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThrows;
import static org.mockito.Mockito.when;

import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;

import com.service.vehicle.exceptions.DataNotFoundException;
import com.service.vehicle.model.Materials;
import com.service.vehicle.service.MaterailService;


@SpringBootTest(classes = {MaterailTest.class})
public class MaterailTest {

	@Mock
	MaterailService materailservice;
	
	@InjectMocks
	Materails  materails;
	
	@Test
	@Order(1)
	public void testSaveMaterail() {
		Materials save = new Materials(101, "tvs", 20, "MRF", 10);
		when(materailservice.storematerials(save)).thenReturn(save);
		assertEquals(save, materails.store(save));

	}

	@Test
	@Order(2)
	public void testGetMaterailDetails() {
		Materials getmaterails = new Materials(101, "tvs", 20, "MRF", 10);
		int branch_id = 101;
		when(materailservice.getmaterails(branch_id)).thenReturn(getmaterails);
		assertEquals(getmaterails, materails.store(branch_id));
		when(materailservice.getmaterails(branch_id)).thenReturn(null);
		assertThrows(DataNotFoundException.class,()->{materails.store(branch_id);});
	}

	@Test
	@Order(3)
	public void testlStockUpdateDetails() {
		Materials updatematerails = new Materials(103, "tvs", 20, "MRF", 10);
		int branch_id = 103;
		when(materailservice.updateDetails(updatematerails, branch_id)).thenReturn(updatematerails);// Mocking
		assertEquals("stock updated", materails.materailUpdate(branch_id, updatematerails));
		when(materailservice.updateDetails(updatematerails, branch_id)).thenReturn(null);
		assertThrows(DataNotFoundException.class,()->{materails.materailUpdate(branch_id, updatematerails);});
	}
}
