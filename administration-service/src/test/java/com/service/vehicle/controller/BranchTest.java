package com.service.vehicle.controller;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThrows;
import static org.mockito.Mockito.when;

import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;

import com.service.vehicle.exceptions.DataNotFoundException;
import com.service.vehicle.model.Branches;
import com.service.vehicle.service.Branch;

@SpringBootTest(classes = {BranchTest.class})
public class BranchTest {

	@Mock
	Branch branchservice;
	
	@InjectMocks
	BranchController branchcontroller;
	
	@Test
	@Order(1)
	public void testSaveBranchDetails() {
		Branches branchdetails = new Branches(101, "kothapet",
				"nagol", 9181798);
		when(branchservice.providied(branchdetails)).thenReturn(branchdetails);
		assertEquals(branchdetails, branchcontroller.address(branchdetails));

	}

	@Test
	@Order(2)
	public void testGetBranchDetails() {
		Branches getbranch = new Branches(101, "kothapet",
				"nagol", 9181798);
		int branch_id = 101;
		when(branchservice.getbranch(branch_id)).thenReturn(getbranch);
		assertEquals(getbranch, branchcontroller.servicerequest(branch_id));
		
		when(branchservice.getbranch(branch_id)).thenReturn(null);
		assertThrows(DataNotFoundException.class,()->{branchcontroller.servicerequest(branch_id);});

	}

	@Test
	@Order(3)
	public void testBranchUpdateDetails() {
		Branches branchupdate = new Branches(101, "uppal",
				"hayath nagar", 3202876);
		int branch_id = 101;
		when(branchservice.updateDetails(branchupdate, branch_id)).thenReturn(branchupdate);// Mocking
		assertEquals(branchupdate, branchcontroller.addressUpdate(branch_id, branchupdate));
		when(branchservice.updateDetails(branchupdate, branch_id)).thenReturn(null);
		assertThrows(DataNotFoundException.class,()->{ branchcontroller.addressUpdate(branch_id, branchupdate);});
	}

}
