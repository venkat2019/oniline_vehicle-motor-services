package com.service.vehicle;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThrows;
import static org.mockito.Mockito.when;

import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.service.vehicle.exceptions.DataNotFoundException;
import com.service.vehicle.model.Materials;
import com.service.vehicle.repository.MaterailDao;
import com.service.vehicle.service.MaterailService;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = { BillingTest.class })
public class MaterailsTest {

	@Mock
	MaterailDao materaildao;

	@InjectMocks
	MaterailService materailservice;

	@Test
	@Order(1)
	public void testSaveMaterials() {
		Materials materail = new Materials(103, "tvs", 10, "mrf", 8);
		when(materaildao.save(materail)).thenReturn(materail);// Mocking
		assertEquals(materail, materailservice.storematerials(materail));
	}

	@Test
	@Order(2)
	public void testGetStoreMaterials() {

		Materials materaildetails = new Materials(101, "tvs", 10, "CEAT", 23);
		int branch_id = 101;
		when(materaildao.findById(branch_id)).thenReturn(materaildetails);// Mocking
		assertEquals(materaildetails,materailservice.getmaterails(branch_id));
	}

	@Test
	@Order(3)
	public void testUpdateStoreMaterails() {
		Materials updatestorematerail = new Materials(101, "tvs", 10, "castro", 23);
		int branch_id = 101;
		String tyre_brand = "tvs";
		int tyre_quantity = 10;
		String engine_oil = "castro";
		int engine_oil_quantity = 23;
		when(materaildao.findById(branch_id)).thenReturn(updatestorematerail);// Mocking
		when(materaildao.save(updatestorematerail)).thenReturn(updatestorematerail);// Mocking
		assertEquals(tyre_brand, materailservice.updateDetails(updatestorematerail, branch_id).getTyrebrand());
		assertEquals(branch_id, materailservice.updateDetails(updatestorematerail, branch_id).getBranch_id());
		assertEquals(tyre_quantity, materailservice.updateDetails(updatestorematerail, branch_id).getTyrebrand_quantity());
		assertEquals(engine_oil, materailservice.updateDetails(updatestorematerail, branch_id).getEngine_oil());
		assertEquals(engine_oil_quantity, materailservice.updateDetails(updatestorematerail, branch_id).getEngine_oil_quantity());
		when(materaildao.findById(branch_id)).thenReturn(null);
		assertThrows(DataNotFoundException.class,()->{materailservice.updateDetails(updatestorematerail, branch_id);});
	}

}
